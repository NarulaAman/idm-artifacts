# README #

### Background ###

All APIs /dowjones-index/** are protected and JWT access token must be provided. If authentication fails because of missing or invalid token, you should expect an error below

    Example - If signature doesn't match.
  
    {
    "statusCd": 403,
    "type": "INVALID_ACCESS_TOKEN",
    "title": INVALID_ACCESS_TOKEN,
    "detail": " Signature Invalid "
    }

#### authenticate - POST /authenticate ####
    Authenticates the user and provides jwt access token
	
	Successful Authentication - HTTP Code 200
	
	{ "jwtToken": "j.w.t" }
	
	Unsuccessfull Authentication - HTTP Code 403
	
	{
    "statusCd": 403,
    "type": "INVALID_CREDENTIALS",
    "title": "INVALID_CREDENTIALS",
    "detail": "Invalid credentials."
    }
	
	
#### get-index-data-by-ticker - GET /dowjones-index/records/ticker ####
    Provides data set for a specific ticker.
	
    Success Response
	
	{
    "dataSet": [
        {
            "id": 751,
            "quarter": 4,
            "stock": "RBC",
            "date": "1/7/2021",
            "open": "$12.12",
            "high": "$14.15",
            "low": "$12.00",
            "close": "$12.75",
            "volume": "345454",
            "percentChangePrice": "3.4",
            "percentChangeVolOvrLastWk": "2.4",
            "previousWkVol": "353534",
            "nextWkOpen": "$12.67",
            "nextWkClose": "$12.89",
            "percentChangeNextWkPrice": "1.8",
            "daysDividend": 4,
            "percentReturnNextDividend": "2"
        },
        {
            "id": 752,
            "quarter": 4,
            "stock": "RBC",
            "date": "1/7/2021",
            "open": "$12.12",
            "high": "$14.15",
            "low": "$12.00",
            "close": "$12.75",
            "volume": "345454",
            "percentChangePrice": "3.4",
            "percentChangeVolOvrLastWk": "2.4",
            "previousWkVol": "353534",
            "nextWkOpen": "$12.67",
            "nextWkClose": "$12.89",
            "percentChangeNextWkPrice": "1.8",
            "daysDividend": 4,
            "percentReturnNextDividend": "2"
        }
    ]
    }
		
	
#### get-index-data-by-ticker - GET /dowjones-index/records/invalid-ticker ####
    
    {
    "statusCd": 404,
    "type": "INDEX_TICKER_NOT_FOUND",
    "title": "INDEX_TICKER_NOT_FOUND",
    "detail": "Index ticker INVALID not found"
    }
   
#### upload-index-data-set - POST /dowjones-index/records ####

     201 Created 
     
#### upload-index-record - POST /dowjones-index/records ####

     201 Created
  
### Contacts ###

* amand.narula@gmail.com 